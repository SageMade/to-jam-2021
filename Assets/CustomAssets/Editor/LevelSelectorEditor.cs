﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// Provides editor support for dragging scene assets onto a LevelSelector component
/// </summary>
[CustomPropertyDrawer(typeof(LevelAttribute))]
public class LevelSelectorEditor : PropertyDrawer
{
    private string _ActualPath;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        string selector = property.stringValue;
        SceneAsset oldScene = null;
        // If we don't already know the asset path
        if (_ActualPath == null)
        {
            // If the target actually has a value set, we need to look for that scene
            if (!string.IsNullOrWhiteSpace(selector))
            {
                // Search for SceneAssets that match the name stored in the selector
                string[] guids = AssetDatabase.FindAssets(string.Format("{0} t:SceneAsset", selector));
                // As long as we found at least one asset that matches
                if (guids.Length > 0)
                {
                    // Load the scene asset using that asset's path (we take the first one)
                    oldScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(AssetDatabase.GUIDToAssetPath(guids[0]));
                }
            }
        }
        // If we already know the path, simply load the asset
        else
        {
            oldScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(_ActualPath);
        }

        // We'll have a label to display the underlying value, just to be safe
        //EditorGUILayout.LabelField("Actual Value: " + selector);

        // Start listening for changes to the editor
        EditorGUI.BeginChangeCheck();
        // Draw the drop target for scenes
        var newScene = EditorGUI.ObjectField(position, oldScene, typeof(SceneAsset), false) as SceneAsset;

        // If one of our properties has been modified
        if (EditorGUI.EndChangeCheck())
        {
            // Get and store the full path of the asset
            string newPath = AssetDatabase.GetAssetPath(newScene);
            _ActualPath = newPath;
            // Determine the scene name from the path filename
            newPath = System.IO.Path.GetFileNameWithoutExtension(newPath);

            property.stringValue = newPath;
        }
    }
}
