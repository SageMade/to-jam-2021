using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputBufferManager : MonoBehaviour
{
    //
    public SO_InputBufferSettings inputBufferSettings;

    //
    public int bufferWindow = 20;
    public List<InputBuffer> bufferCollection;

    //
    public List<int> checkControllerInput;
    public List<int> checkMotionCommand;

    //
    public bool drawDebug = false;

    // Start is called before the first frame update
    void Start()
    {
        // Initialize Buffers
        bufferCollection = new List<InputBuffer>();
        for (int i = 0; i < inputBufferSettings.bufferedInputs.Count; i++)
        {
            // Populate the buffer collection with buffers for each buffered input
            InputBuffer newInputBuffer = new InputBuffer();
            // Also populate each individual buffer with their storage frames
            newInputBuffer.buffer = new List<BufferState>();
            for (int j = 0; j < bufferWindow; j++)
            {
                BufferState newBufferState = new BufferState();
                newInputBuffer.buffer.Add(newBufferState);
            }
            bufferCollection.Add(newInputBuffer);
        }

        // This list stores the status of each buffered input
        checkControllerInput = new List<int>();
        for (int i = 0; i < bufferCollection.Count; i++)
        {
            checkControllerInput.Add(-1);
        }
        // This list is only for motion commands
        checkMotionCommand = new List<int>();
        for (int i = 0; i < inputBufferSettings.motionCommands.Count; i++)
        {
            checkMotionCommand.Add(-1);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    // Called every 0.01666667 ~60fps
    void FixedUpdate()
    {
        // Update the buffers
        for (int i = 0; i < bufferCollection.Count; i++)
        {
            for (int j = 0; j < (bufferWindow - 1); j++)
            {
                // Push the old values up
                bufferCollection[i].buffer[j].rawValue = bufferCollection[i].buffer[j + 1].rawValue;
                bufferCollection[i].buffer[j].inputState = bufferCollection[i].buffer[j + 1].inputState;
                bufferCollection[i].buffer[j].used = bufferCollection[i].buffer[j + 1].used;
            }
        }
        // Check for new controller input and enter it into the buffer
        for (int i = 0; i < bufferCollection.Count; i++)
        {
            //bufferCollection[i].buffer[bufferWindow - 1]
            switch (inputBufferSettings.bufferedInputs[i].inputType)
            {
                case ControllerInput.InputType.BUTTON:
                    bufferCollection[i].buffer[bufferWindow - 1].used = false;
                    if (Input.GetButton(inputBufferSettings.bufferedInputs[i].inputName))
                    {
                        bufferCollection[i].buffer[bufferWindow - 1].SetInput(1);
                    }
                    else
                    {
                        bufferCollection[i].buffer[bufferWindow - 1].ReleaseInput();
                    }
                    break;
                case ControllerInput.InputType.AXIS:
                    bufferCollection[i].buffer[bufferWindow - 1].used = false;
                    if (Mathf.Abs(Input.GetAxisRaw(inputBufferSettings.bufferedInputs[i].inputName)) > 0)   // Any axis input surpassing Unity's deadzone setting
                    {
                        bufferCollection[i].buffer[bufferWindow - 1].SetInput(Input.GetAxisRaw(inputBufferSettings.bufferedInputs[i].inputName));
                    }
                    else
                    {
                        bufferCollection[i].buffer[bufferWindow - 1].ReleaseInput();
                    }
                    break;
                case ControllerInput.InputType.IGNORE:
                    // Do nothing
                    break;
            }
        }

        // Update available inputs and motion commands
        bool earlyStop;
        for (int i = 0; i < checkControllerInput.Count; i++)
        {
            checkControllerInput[i] = -1;
            earlyStop = false;
            for (int j = 0; j < (bufferWindow - 1) && earlyStop == false; j++)
            {
                if (bufferCollection[i].buffer[j].InputReady() == true)
                {
                    checkControllerInput[i] = j;
                    earlyStop = true;
                }
            }
        }
        //
        earlyStop = false;
        for (int i = 0; i < checkMotionCommand.Count; i++)
        {
            if (checkMotionCommand[i] >= 0)
            {
                checkMotionCommand[i]--;
            }

            // If motion command is not available for use, check to see if it's available
            // NOTE: CONSIDER ONLY CHECKING FOR AVAILABLE COMMANDS IF THE CHAR IS IN A STATE THAT COULD USE ONE
            if (checkMotionCommand[i] == -1)
            {
                int commandStep = 0;
                int bufferStage = 0;
                for (int j = 0; j < inputBufferSettings.motionCommands[i].commands.Count; j++)
                {
                    // For each of the commands, check the buffer to find a matching input
                    for (int x = bufferStage; x < (bufferWindow - 1) && earlyStop == false; x++) // Start at the bufferStage. if a command is found, the search for the next command starts after the previous command
                    {
                        if (inputBufferSettings.motionCommands[i].commands[j] == CheckDirection(bufferCollection[1].buffer[x].rawValue, bufferCollection[2].buffer[x].rawValue))
                        {
                            // we found a matching direction. increment and check the next command step
                            bufferStage = x;
                            commandStep++;
                            break;
                        }
                    }
                    if (commandStep == inputBufferSettings.motionCommands[i].commands.Count)
                    {
                        // Set availability of the motion command for X frames
                        checkMotionCommand[i] = inputBufferSettings.motionCommandActiveFrames;
                        earlyStop = true;
                    }
                }
            }
        }
    }

    //
    MotionCommand.MotionDirection CheckDirection(float stickX, float stickY)
    {
        Vector3 playerForward = GameEngine.instance.ActiveCharacter.transform.forward.normalized;
        //Debug.DrawRay(GameEngine.instance.ActiveCharacter.transform.position, playerForward * 10, Color.green);

        Vector3 stickDirection = new Vector3(0, 0, 0);
        Vector3 camDirF = GameEngine.instance.ActiveCamera.transform.forward.normalized; camDirF.y = 0;
        Vector3 camDirR = GameEngine.instance.ActiveCamera.transform.right.normalized; camDirR.y = 0;
        stickDirection += camDirF * stickY;
        stickDirection += camDirR * stickX;
        stickDirection.Normalize();
        //Debug.DrawRay(GameEngine.instance.ActiveCharacter.transform.position, stickDirection * 10, Color.blue);

        float angleDiff = Vector3.SignedAngle(playerForward, stickDirection, Vector3.up);

        if (stickX == 0 && stickY == 0)
        {
            return MotionCommand.MotionDirection.NEUTRAL;
        }
        else if (angleDiff > -45 && angleDiff < 45)
        {
            return MotionCommand.MotionDirection.FORWARD;
        }
        else if (angleDiff < -45 && angleDiff > -135)
        {
            return MotionCommand.MotionDirection.LEFT;
        }
        else if (angleDiff > 45 && angleDiff < 135)
        {
            return MotionCommand.MotionDirection.RIGHT;
        }
        else
        {
            return MotionCommand.MotionDirection.BACK;
        }
    }

    // Use up input
    public void UseInput(int a_index)
    {
        if (checkControllerInput[a_index] > 0)
        {
            bufferCollection[a_index].buffer[checkControllerInput[a_index]].used = true;
            checkControllerInput[a_index] = -1;
        }
    }

    // DEBUG
    void OnGUI()
    {
        if (drawDebug)
        {
            int xSpace = 40;
            int ySpace = 15;
            for (int x = 0; x < checkControllerInput.Count; x++)
            {
                GUI.Label(new Rect(20 + (x * xSpace), 5, 30, 20), checkControllerInput[x].ToString());
            }
            for (int x = 0; x < bufferCollection.Count; x++)
            {
                for (int y = 0; y < bufferWindow; y++)
                {
                    if (bufferCollection[x].buffer[y].inputState >= 1)
                    {
                        GUI.color = Color.green;
                    }
                    else
                    {
                        GUI.color = Color.white;
                    }
                    if (bufferCollection[x].buffer[y].used == true)
                    {
                        GUI.color = Color.red;
                    }
                    GUI.Label(new Rect(20 + (x * xSpace), 30 + (y * ySpace), 30, 20), bufferCollection[x].buffer[y].inputState.ToString());
                }
            }
            //
            for (int x = 0; x < checkMotionCommand.Count; x++)
            {
                GUI.color = Color.magenta;
                GUI.Label(new Rect(400, (5 + (x * ySpace)), 200, 20), inputBufferSettings.motionCommands[x].commandName);
            }
            for (int x = 0; x < checkMotionCommand.Count; x++)
            {
                if (checkMotionCommand[x] >= 1)
                {
                    GUI.color = Color.blue;
                }
                else
                {
                    GUI.color = Color.white;
                }
                GUI.Label(new Rect(550, 5 + (x * ySpace), 30, 20), checkMotionCommand[x].ToString());
            }
        }
    }
}

public class InputBuffer
{
    public List<BufferState> buffer;
}

public class BufferState
{
    public float rawValue;
    public int inputState;
    public bool used;

    public void SetInput(float a_value)
    {
        //
        rawValue = a_value;
        if (inputState < 0)
        {
            inputState = 1;
        }
        else
        {
            inputState += 1;
        }
    }

    public void ReleaseInput()
    {
        //
        rawValue = 0;
        if (inputState > 0)
        {
            inputState = -1;
            used = false;
        }
        else
        {
            inputState = 0;
        }
    }

    public bool InputReady()
    {
        //
        if (inputState == 1 && used == false)
        {
            return true;
        }
        return false;
    }
}