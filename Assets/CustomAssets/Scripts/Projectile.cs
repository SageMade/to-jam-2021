using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Jakob
{
    public abstract class Projectile : MonoBehaviour
    {
        //Called once, when the projectile hits something
        public UnityEvent<Projectile, RaycastHit> onHit;

        //Direction and speed of projectile, likely will be set in whatever fires it
        public Vector3 direction = Vector3.up;
        public float speed = 10f;

        //Mask to let us avoid certain collisions
        public LayerMask rayMask;

        //Moves the projectile one step through the world
        private void MoveProjectile()
        {
            float step = Time.fixedDeltaTime * speed;

            RaycastHit rch;

            if (Physics.Raycast(transform.position, direction, out rch, step, rayMask))
            {
                transform.position = rch.point;

                onHit?.Invoke(this, rch);
            }
            else
            {
                transform.position = transform.position + direction * step;
            }
        }

        //Normal fixed update
        protected void FixedUpdate()
        {
            MoveProjectile();

            OnFixedUpdate();
        }

        //Placed an overridable function here so people can override it without accidentally stopping the normal projectile behaviour
        protected virtual void OnFixedUpdate()
        {

        }
    }
}