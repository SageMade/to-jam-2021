using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jakob
{
    public class GenericTestGun : MonoBehaviour
    {
        public GenericTestBullet testProj;
        public Transform barrel;

        public float timer = 1f;
        private float internal_time = 0f;

        public float bulletSpeed = 10f;

        private void Update()
        {
            internal_time -= Time.deltaTime;

            if (internal_time <= 0)
            {
                internal_time = timer;

                GenericTestBullet firedProj = Instantiate(testProj);

                firedProj.transform.position = barrel.position;
                firedProj.direction = barrel.forward;
                firedProj.speed = bulletSpeed;

                firedProj.onHit.AddListener(OnHit);
            }
        }

        protected void OnHit(Projectile proj, RaycastHit rch)
        {
            Destroy(proj.gameObject);
        }
    }
}