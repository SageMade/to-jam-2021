using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CharacterBase : MonoBehaviour
{
    //
    public SO_CharacterState currentState;
    public float stateTimeCurrent = 0;
    public float stateTimePrevious = -1;

    //
    public GameObject character;
    public Animator animator;
    public CharacterController charController;

    //
    public Vector3 velocity;
    public Vector3 friction = new Vector3(0.75f, 0.75f, 0.75f);
    public float gravity = -0.05f;

    // TEMP TEMP TEMP TEMP
    public bool isAerial = true;
    public int faceDir = 1;
    public GameObject hitbox;

    // Health between 0 and 1
    [Range(0.0f, 1.0f)]
    public float health = 1.0f;

    public FMODUnity.StudioEventEmitter SFX_jump;
    public FMODUnity.StudioEventEmitter SFX_attack;

    // Start is called before the first frame update
    void Start()
    {
        charController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        // TEMP TEMP TEMP TEMP
    }

    // Called every 0.01666667 ~60fps
    void FixedUpdate()
    {
        // Update state transitions
        UpdateStateTransitions();
        // Update state and state events
        UpdateCharacterState();
        // Update character physics
        UpdateCharacterPhysics();
    }

    // Base > handles inputs and changes states. Child > child specific state transitions like overdrive meter, stance switches, etc.
    protected virtual void UpdateStateTransitions()
    {
        //
        foreach (SO_CharacterState.StateTransition _st in currentState.stateTransitions)
        {
            //
            if (stateTimeCurrent >= _st.checkStart && stateTimeCurrent <= _st.checkEnd)
            {
                //
                if (_st.enable)
                {
                    bool transition = true;
                    bool denied = false;            // This is probably a stupid ass way to do this, but I can't brain right now. revisit this later and fix it

                    //
                    foreach (int motion in _st.motionCommands)
                    {
                        transition = CheckMotion(motion);
                        if (transition == false)
                        {
                            denied = true;
                        }
                    }
                    //
                    foreach (int input in _st.simpleInputs)
                    {
                        transition = CheckInput(input);
                        if (transition == false)
                        {
                            denied = true;
                        }
                    }
                    // TEMP TEMP TEMP TEMP
                    //
                    foreach (int custom in _st.customChecks)
                    {
                        transition = CheckCustom(custom);
                        if (transition == false)
                        {
                            denied = true;
                        }
                    }

                    // All checks completed and confirmed.
                    if (transition && denied == false)
                    {
                        // Use up all the imputs required for the state transition
                        foreach (int input in _st.simpleInputs)
                        {
                            GameEngine.instance.inputBufferManager.UseInput(input);
                        }
                        StartState(_st.nextState);
                    }
                }
            }
        }
    }

    // \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ 
    #region TransitionChecks
    //+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x
    //
    bool CheckMotion(int a_motion)
    {
        //
        switch (a_motion)
        {
            //case 88: if (GameEngine.instance.inputBufferManager.checkMotionCommand[0] > 0) { return true; } break;
            //case 28: if (GameEngine.instance.inputBufferManager.checkMotionCommand[1] > 0) { return true; } break;
            default:
                return false;
        }
        //return false;
    }
    //
    bool CheckInput(int a_input)
    {
        //
        switch (a_input)
        {
            case 3: if (GameEngine.instance.inputBufferManager.checkControllerInput[3] > 0) { return true; } break;
            case 4: if (GameEngine.instance.inputBufferManager.checkControllerInput[4] > 0) { return true; } break;
            case 5: if (GameEngine.instance.inputBufferManager.checkControllerInput[5] > 0) { return true; } break;
            case 6: if (GameEngine.instance.inputBufferManager.checkControllerInput[6] > 0) { return true; } break;
            default:
                return false;
        }
        return false;
    }
    // TEMP TEMP TEMP TEMP
    bool CheckCustom(int a_custom)
    {
        //
        switch (a_custom)
        {
            case 0: // FallToNeutral
                if (!isAerial) { return true; } break;
            default:
                return false;
        }
        return false;
    }
    //+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x
    #endregion
    // /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ 

    //
    public void StartState(SO_CharacterState a_newState, float timeCurrent = 0)
    {
        currentState = a_newState;
        stateTimeCurrent = timeCurrent;
        stateTimePrevious = timeCurrent - 1;
        //
        animator.CrossFadeInFixedTime(a_newState.animState, a_newState.animBlendRate);
        animator.speed = (a_newState.animSpeed);
    }

    //
    void UpdateCharacterState()
    {
        //
        UpdateStateEvents();

        //
        stateTimePrevious = stateTimeCurrent;
        stateTimeCurrent++;

        //
        if (stateTimeCurrent >= currentState.frameLength)
        {
            // Loop current state if set to loop
            if (currentState.loopState)
            {
                //
                stateTimeCurrent = 0;
                stateTimePrevious = -1;
            }

            // TEMP TEMP TEMP TEMP
            if (currentState.autoTransition != null)
            {
                StartState(currentState.autoTransition);
            }
        }
    }

    //
    protected virtual void UpdateStateEvents()
    {
        //
        foreach (SO_CharacterState.StateEvent _se in currentState.stateEvents)
        {
            //
            if (stateTimeCurrent >= _se.eventStart && stateTimeCurrent <= _se.eventEnd)
            {
                //
                if (_se.enable)
                {
                    //
                    RunBaseEvent(_se.script.eventID, _se.scriptParams);
                }
            }
        }
    }

    //
    protected void RunBaseEvent(int eventID, List<Vector3> sParams)
    {
        switch (eventID)
        {
            case 0: // StickMovementXAxis
                SE_StickMovementXAxis(sParams); break;
            case 1: // AddXYVelocity
                SE_AddXYVelocity(sParams); break;
            case 2: // FaceStickDirectionLR
                SE_FaceStickDirectionLR(sParams); break;
            case 3: // FacingDirectionCorrection
                SE_FacingDirectionCorrection(sParams); break;
            case 4: // FaceDirVelocity
                SE_FaceDirVelocity(sParams); break;
            case 5: // SetHitbox
                SE_SetHitbox(sParams); break;
            case 6: // ToggleLayer
                SE_ToggleLayer(sParams); break;
            case 7: // PlayTheSound << JANK JANK JANK JANK JANK JANK JANK JANK JANK JANK JANK JANK JANK JANK JANK
                SE_PlayTheSound(sParams); break;
            default:
                break;
        }
    }

    // \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ 
    #region StateEvents
    //+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x+x
    //
    void SE_StickMovementXAxis(List<Vector3> sParams)
    {
        if(Mathf.Abs(Input.GetAxis("P1LeftStickX")) > GameEngine.instance.stickDeadzone)
        {
            //velocity += new Vector3(Input.GetAxis("P1LeftStickX"), 0, 0) * sParams[0].x;
            velocity += new Vector3(Mathf.Sign(Input.GetAxis("P1LeftStickX")) * 1, 0, 0) * sParams[0].x;

            //
            animator.SetFloat("P1LeftStickX", Mathf.Abs(Input.GetAxis("P1LeftStickX")));
        }
        else
        {
            animator.SetFloat("P1LeftStickX", 0);
        }
    }

    //
    void SE_AddXYVelocity(List<Vector3> sParams)
    {
        velocity += new Vector3(sParams[0].x, sParams[0].y, 0);
    }

    //
    void SE_FaceStickDirectionLR(List<Vector3> sParams)
    {
        // 179 <- Y -> 1
        if (Mathf.Abs(Input.GetAxis("P1LeftStickX")) > GameEngine.instance.stickDeadzone)
        {
            if (Input.GetAxis("P1LeftStickX") > 0)
            {
                character.transform.rotation = Quaternion.Lerp(character.transform.rotation, Quaternion.Euler(0, 1, 0), sParams[0].x);
                faceDir = 1;
            }
            else if (Input.GetAxis("P1LeftStickX") < 0)
            {
                character.transform.rotation = Quaternion.Lerp(character.transform.rotation, Quaternion.Euler(0, 179, 0), sParams[0].x);
                faceDir = -1;
            }
        }
    }

    //
    void SE_FacingDirectionCorrection(List<Vector3> sParams)
    {
        if (Mathf.Abs(Input.GetAxis("P1LeftStickX")) < GameEngine.instance.stickDeadzone || isAerial)
        {
            if (faceDir == 1 && character.transform.rotation.y != 1)
            {
                character.transform.rotation = Quaternion.Lerp(character.transform.rotation, Quaternion.Euler(0, 1, 0), sParams[0].x);
            }
            else if (faceDir == -1 && character.transform.rotation.y != 179)
            {
                character.transform.rotation = Quaternion.Lerp(character.transform.rotation, Quaternion.Euler(0, 179, 0), sParams[0].x);
            }
        }
    }

    //
    void SE_FaceDirVelocity(List<Vector3> sParams)
    {
        velocity += new Vector3(sParams[0].x * faceDir, sParams[0].y, 0);
    }

    //
    void SE_SetHitbox(List<Vector3> sParams)
    {
        switch (sParams[0].x)
        {
            case 0: hitbox.gameObject.SetActive(false);
                break;
            case 1: hitbox.gameObject.SetActive(true);
                break;
            default:
                break;
        }
    }

    //
    void SE_ToggleLayer(List<Vector3> sParams)
    {
        if (sParams[0].x == 3) // Set to normal
        {
            if (Physics.CheckSphere(transform.position, 0.75f, LayerMask.GetMask("Enemy")))
            {
                return;
            }
        }
        gameObject.layer = (int)sParams[0].x;
    }

    //
    void SE_PlayTheSound(List<Vector3> sParams)
    {
        switch (sParams[0].x)
        {
            case 0:
                SFX_jump.Play();
                break;
            case 1:
                SFX_attack.Play();
                break;
            default:
                break;
        }
    }

    //void SE_CameraRelativeMovement(List<Vector3> sParams)
    //{
    //    // Get Direction based on camera and player stick input
    //    Vector3 cameraDirection = GameEngine.instance.ActiveCamera.transform.forward;
    //    Vector3 movementDirection = new Vector3(0, 0, 0);
    //
    //    // Normalize the direction
    //    cameraDirection.y = 0;
    //    cameraDirection.Normalize();
    //
    //    movementDirection += cameraDirection * Input.GetAxis("P1LeftStickY");
    //    movementDirection += GameEngine.instance.ActiveCamera.transform.right * Input.GetAxis("P1LeftStickX");
    //    movementDirection.y = 0;
    //
    //    // Multiply by speed and add to character velocity
    //    velocity += (movementDirection * sParams[0].x);
    //}

    #endregion
    // /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ 

    //
    void UpdateCharacterPhysics()
    {
        //
        velocity.y += gravity;

        // Update Flags
        if ((charController.collisionFlags & CollisionFlags.Below) != 0)
        {
            isAerial = false;
        }
        else
        {
            isAerial = true;
        }

        //
        charController.Move(velocity);
        velocity.Scale(friction);
    }
}
