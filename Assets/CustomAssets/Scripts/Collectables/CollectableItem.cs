using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CollectableItem : MonoBehaviour
{
    [Range(0.0f, 100.0f)]
    public float GenerationPercentage = 10.0f;
    public float RotationSpeed = 90.0f;
    public float BounceRate = 1.0f;
    public float BounceDistance = 0.5f;

    public UnityEngine.UI.Image SpriteRenderer;

    private Collectable _item;
    private float _originY;

    // Start is called before the first frame update
    void Start()
    {
        if (Random.Range(0, 1.0f) > GenerationPercentage / 100.0f) {
            Destroy(gameObject);
            return;
        }

        Collectable item = Collectable.GetAll().Where(x => {
            return PlayerPrefs.GetInt(x.PrefName, 0) == 0;
        }).TakeRandom();

        if (item == null) {
            Destroy(gameObject);
            Debug.LogWarning("No collectibles left!");
            return;
        }
        _item = item;

        SpriteRenderer.sprite = item.Icon;
        _originY = transform.position.y;
    }

    // Update is called once per frame
    void Update() {
        transform.Rotate(Vector3.up * RotationSpeed * Time.deltaTime, Space.Self);
        Vector3 pos = transform.position;
        pos.y = _originY + Mathf.Sin(Time.timeSinceLevelLoad * BounceRate) * BounceDistance;
        transform.position = pos;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            PlayerPrefs.SetInt(_item.PrefName, 1);
            PlayerPrefs.Save();
            GameEngine.CurrentRun.CollectedItems.Add(_item);
            // TODO: Play SFX
            Destroy(gameObject);
        }
    }
}
