using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableGridController : MonoBehaviour
{
    public GameObject CardPrefab;

    // Start is called before the first frame update
    void Awake()
    {
        Collectable[] items = Collectable.GetAll();

        foreach(Collectable item in items) {
            GameObject card = Instantiate(CardPrefab, transform);
            CollectableCardController controller = card.GetComponent<CollectableCardController>();
            controller.Item = item;
            controller.Unlocked = PlayerPrefs.HasKey(item.PrefName) && PlayerPrefs.GetInt(item.PrefName) == 1;
        }
    }
}
