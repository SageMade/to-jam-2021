using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectableCardController : MonoBehaviour
{
    public Image Icon;
    public TMPro.TextMeshProUGUI Text;

    public Collectable Item;

    public bool Unlocked;

    void Start() {
        if (Item) {
            Icon.sprite = Item.Icon;
            Icon.color = Unlocked ? Color.white : Color.black;
            Text.text = Unlocked ? Item.Name : "???";
        }
    }
}
