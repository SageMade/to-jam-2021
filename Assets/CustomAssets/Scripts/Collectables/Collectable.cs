using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Custom/Collectable")]
public class Collectable : ScriptableObject
{
    public Sprite Icon;
    public string Name;

    public string PrefName {
        get { return Name.ToLower().Replace(' ', '_'); }
    }

    private static Collectable[] _items;
    public static Collectable[] GetAll() {
        _items = _items ?? Resources.LoadAll<Collectable>("Collectables");
        return _items;
    }
}
