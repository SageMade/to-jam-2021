using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TEMP_Enemy : MonoBehaviour
{
    public GameObject target;
    public float hp = 3;
    public GameObject bullet;
    public bool active = false;
    public float shootTimer = -1;
    public float shootCD = 3;

    // Start is called before the first frame update
    void Start()
    {
        target = GameEngine.instance.ActiveCharacter;
    }

    // Update is called once per frame
    void Update()
    {
        if (hp <= 0)
        {
            gameObject.SetActive(false);
        }
        //Debug.Log("Distance: " + (target.transform.position - transform.position).magnitude);
        if ((target.transform.position - transform.position).magnitude < 20)
        {
            //Debug.Log("I SMELL BLOOD");
            active = true;
        }
        else
        {
            active = false;
        }
        if (active && shootTimer <= 0)
        {
            // shoot bullet
            GameObject newBullet = Instantiate(bullet, transform.position, Quaternion.identity);
            newBullet.GetComponent<TEMP_Boolet>().direction = (target.transform.position - transform.position).normalized;
            newBullet.GetComponent<TEMP_Boolet>().enable = true;
            newBullet.GetComponent<TEMP_Boolet>().speed = 4;
            shootTimer = shootCD;
        }
        if (shootTimer > 0)
        {
            shootTimer -= Time.deltaTime;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Hitbox")
        {
            hp--;
        }
        //Debug.Log("Touching: " + other.name);
    }
}
