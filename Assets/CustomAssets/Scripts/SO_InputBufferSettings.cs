using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Input Buffer Settings", menuName = "Scriptable Objects/Input Buffer Settings", order = 2)]
[System.Serializable]
public class SO_InputBufferSettings : ScriptableObject
{
    public List<ControllerInput> bufferedInputs;
    public int motionCommandActiveFrames = 9;
    public List<MotionCommand> motionCommands;
}

[System.Serializable]
public class ControllerInput
{
    public string inputName;
    public enum InputType { BUTTON, AXIS, IGNORE }
    public InputType inputType;
}

[System.Serializable]
public class MotionCommand
{
    public string commandName;
    public enum MotionDirection { NEUTRAL, FORWARD, BACK, LEFT, RIGHT }
    public List<MotionDirection> commands;
}