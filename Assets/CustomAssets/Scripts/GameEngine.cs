using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEngine : MonoBehaviour
{
    public class GameStats
    {
        public float CompletionTime;
        public int Rewinds;
        public List<Collectable> CollectedItems = new List<Collectable>();
    }

    public static GameStats CurrentRun = new GameStats();

    //
    public static GameEngine instance;

    //
    public InputBufferManager inputBufferManager;

    //
    public GameObject ActiveCharacter;
    public GameObject ActiveCamera;

    //
    public float stickDeadzone = 0.10f;

    // Difficulty multiplier
    public static int Difficulty = 1;

    //
    void Awake()
    {
        if (instance == null) {
            instance = this;
        } else {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
