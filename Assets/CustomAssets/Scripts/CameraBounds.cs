using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jakob
{
    public class CameraBounds : MonoBehaviour
    {
        [SerializeField]
        protected List<Collider> bounds;

        [SerializeField]
        protected bool addAllChildCollidersToBoundsOnAwake = false;

        private void Awake()
        {
            foreach (var c in GetComponentsInChildren<Collider>(true))
            {
                bounds.Add(c);
            }
        }

        public Vector3 GetPointInBounds(Vector3 point)
        {
            Vector3 toReturn = point;
            float sqrDist = float.MaxValue;

            foreach(var c in bounds)
            {
                Vector3 p = c.ClosestPoint(point);
                
                if (p == point)
                {
                    return point;
                }

                float sqrMag = (p - point).sqrMagnitude;

                if (sqrMag < sqrDist)
                {
                    toReturn = p;
                    sqrDist = sqrMag;
                }
            }

            return toReturn;
        }
    }
}