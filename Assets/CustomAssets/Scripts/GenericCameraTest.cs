using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jakob
{
    public class GenericCameraTest : MonoBehaviour
    {
        public Transform forwardAxis;
        public float rotationsPerSecond = 1f;

        private void Update()
        {
            //transform.rotation = transform.rotation * Quaternion.AngleAxis(rotationsPerSecond * 360.0f * Time.deltaTime, forwardAxis.forward);
            transform.rotation = Quaternion.AngleAxis(rotationsPerSecond * 360.0f * Time.deltaTime, forwardAxis.forward) * transform.rotation;
        }
    }
}