using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jakob
{
    public class LerpToPoint : MonoBehaviour
    {
        [SerializeField]
        protected Transform target;

        public CameraBounds cBounds;

        [SerializeField]
        protected float epsilon = 0.001f;

        [SerializeField]
        protected float positionLerpStrength = 5.0f;

        [SerializeField]
        protected float rotationLerpStrength = 5.0f;

        private void FixedUpdate()
        {
            LERP(Time.fixedDeltaTime);
        }

        private void LERP(float dt)
        {
            if (target == null)
            {
                return;
            }

            

            Vector3 targPos = target.position;
            Quaternion targRot = target.rotation;

            if (cBounds != null)
            {
                targPos = cBounds.GetPointInBounds(targPos);
            }

            if ((targPos - transform.position).sqrMagnitude <= epsilon * epsilon)
            {
                transform.position = targPos;
            }
            else
            {
                float deltaPos = Mathf.Exp(-dt * positionLerpStrength);
                transform.position = Vector3.Lerp(targPos, transform.position, deltaPos);
            }

            if (Quaternion.Angle(transform.rotation, targRot) <= epsilon)
            {
                transform.rotation = targRot;
            }
            else
            {
                float deltaRot = Mathf.Exp(-dt * rotationLerpStrength);
                transform.rotation = Quaternion.Slerp(targRot, transform.rotation, deltaRot);
            }
        }
    }
}