using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Character State", menuName = "Scriptable Objects/Character State", order = 0)]
[System.Serializable]
public class SO_CharacterState : ScriptableObject
{
    //
    public string stateName;
    public int frameLength; // in frames
    public bool loopState;
    public string animState;
    public AnimationClip animClip;
    public float animLength; // I forget what I was going to use this for. cull it later/eventually
    public float animSpeed;
    public float animBlendRate;

    //
    public List<StateEvent> stateEvents;
    public List<StateTransition> stateTransitions;

    // TEMP TEMP TEMP TEMP
    public SO_CharacterState autoTransition;

    //
    [System.Serializable]
    public class StateEvent
    {
        //
        public bool enable = true;
        public int eventStart;
        public int eventEnd;

        //
        public SO_CharacterStateEvent script;
        // Params for event scripts go here. it'd be nice if I can figure out how to get different params to show up for different events without going super complicated
        // Nope. will need custome editors for something like that. possibly do in the future. not a priority atm
        public List<Vector3> scriptParams;
    }

    //
    [System.Serializable]
    public class StateTransition
    {
        //
        public bool enable = true;
        public SO_CharacterState nextState;
        public int checkStart;
        public int checkEnd;

        //
        public enum commands { DoubleTapForward = 88, BackForward = 28 }
        public List<commands> motionCommands;
        public enum controllerInputs { A = 3, B = 4, X = 5, Y = 6 }
        public List<controllerInputs> simpleInputs;

        // TEMP TEMP TMEP
        public enum custom { FallToNeutral }
        public List<custom> customChecks;
    }
}
