using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicTriggerRegion : MonoBehaviour {
    [FMODUnity.ParamRef]
    public string ParamName;

    [Range(0, 4)]
    public int ParamValue;

    public FMODUnity.StudioEventEmitter BGMEmitter;

    // Update is called once per frame
    void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            BGMEmitter.SetParameter(ParamName, ParamValue);
        }        
    }
}
