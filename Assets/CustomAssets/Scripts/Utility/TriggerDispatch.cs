using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerDispatch : MonoBehaviour
{
    public int LayerMask = int.MaxValue;

    public UnityEvent<Collider> OnTriggerEnterEvent;
    public UnityEvent<Collider> OnTriggerStayEvent;
    public UnityEvent<Collider> OnTriggerExitEvent;

    private void OnTriggerEnter(Collider other) {
        OnTriggerEnterEvent?.Invoke(other);
    }

    private void OnTriggerStay(Collider other) {
        OnTriggerStayEvent?.Invoke(other);
    }

    private void OnTriggerExit(Collider other) {
        OnTriggerExitEvent?.Invoke(other);
    }
}
