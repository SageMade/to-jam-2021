using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RewindQueue : MonoBehaviour
{
    public CharacterBase Character;

    private List<RewindDataEntry> _queue = new List<RewindDataEntry>();
    Coroutine _tracker;
    private Transform _leo;

    public UnityEvent RewindCompletedEvent;

    [Tooltip("The number of snapshots to record every second")]
    public int EntriesPerSecond = 15;

    private float _referenceTime;
    private float _rewindTime;
    private float _accumulator = 0.0f;

    [Tooltip("Check to enable tracking objects immediately on scene load")]
    public bool TrackOnStart;

    public bool IsTracking {
        get;
        set;
    } = false;

    public bool IsRewinding
    {
        get;
        protected set;
    } = false;

    private void Start() {
        _leo = Character.gameObject.transform.GetChild(0);
        if (TrackOnStart) {
            StartTracking();
        }
    }

    public void StartTracking()
    {
        if (!IsRewinding)
        {
            if (_tracker != null) { StopCoroutine(_tracker); }
            IsTracking = true;
            _tracker = StartCoroutine(TrackEntries());
            _referenceTime = Time.realtimeSinceStartup;
        }
    }


    private IEnumerator TrackEntries()
    {
        while (IsTracking)
        {
            yield return new WaitForSecondsRealtime(1.0f / EntriesPerSecond);
            if (!IsRewinding)
            {
                RewindDataEntry entry = new RewindDataEntry();
                entry.Position = Character.transform.position;
                entry.Rotation = Character.transform.rotation;
                entry.State = Character.currentState;
                entry.StateTimeCurrent = Character.stateTimeCurrent;
                entry.TimeStamp = Time.realtimeSinceStartup - _referenceTime;
                entry.PlayerDir = _leo.localRotation;
                _queue.Add(entry);
            }
        }
    }

    public void Update() {
        // If we are rewinding, we need to work our way backwards through the event queues
        if (!IsTracking & IsRewinding) {
            // As long as there's time left in the rewind
            if (_rewindTime > 0.0f) {
                // Walk our rewind time backwards
                _rewindTime -= Time.deltaTime * _accumulator;
                _accumulator += .5f * Time.deltaTime; // Speed up a bit every frame

                // Remove any keys that are already outdated
                while (_queue.Count > 1 && _queue[_queue.Count - 2].TimeStamp > _rewindTime)
                    _queue.RemoveAt(_queue.Count - 1);

                // If we have more than one key left, lerp them
                if (_queue.Count > 1) {
                    // Determine start and end points
                    RewindDataEntry end = _queue[_queue.Count - 1];
                    RewindDataEntry start = _queue[_queue.Count - 2];
                    // Ensure time is within the range of the keys
                    if (end.TimeStamp >= _rewindTime && start.TimeStamp <= _rewindTime) {
                        // Calculate the T value based on the rewinded time
                        float range = end.TimeStamp - start.TimeStamp;
                        float t = Mathf.Clamp01((_rewindTime - start.TimeStamp) / range);
                        // Interpolate position and rotation
                        Character.transform.position = Vector3.Lerp(start.Position, end.Position, t);
                        Character.transform.rotation = Quaternion.Slerp(start.Rotation, end.Rotation, t);

                        // Interpolate the state
                        _leo.localRotation = Quaternion.Slerp(start.PlayerDir, end.PlayerDir, t);
                        Character.StartState(start.State, Mathf.Lerp(start.StateTimeCurrent, end.StateTimeCurrent, t));
                    }
                    // If the timer has passed the start key's time, remove the end key
                    if (_rewindTime < start.TimeStamp)
                        _queue.RemoveAt(_queue.Count - 1);
                }
                // If we have only one key left, jump to it's position
                else if (_queue.Count == 1) {
                    RewindDataEntry entry = _queue[0];
                    Character.transform.position = entry.Position;
                    Character.transform.rotation = entry.Rotation;
                    _leo.localRotation = entry.PlayerDir;
                    Character.StartState(entry.State, entry.StateTimeCurrent);
                    _queue.Clear();
                }
            }
            // We have reached the start again
            else {
                IsRewinding = false;
                _accumulator = 1.0f;
                StartTracking();
                Debug.LogWarning("Rewind time over...");
                RewindCompletedEvent?.Invoke();
            }
        }
    }

    public void Rewind() {
        if (!IsRewinding)
        {
            IsRewinding = true;
            if (_tracker != null) StopCoroutine(_tracker);
            Debug.Log("It's rewind time");
            _tracker = null;
            IsTracking = false;
            _accumulator = 1.0f;
            _rewindTime = Time.realtimeSinceStartup - _referenceTime;
        } else {
            Debug.LogWarning("NOT REWIND TIME ABORT!");
        }
    }
}
