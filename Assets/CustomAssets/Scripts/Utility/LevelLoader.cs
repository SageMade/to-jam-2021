using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public float animTime = 1f;

    public Animator transition;
    public GameObject loadScreen;
    public Slider sliding;


    private void OnLevelWasLoaded(int level)
    {
        transition.Play("FadeFromBlack");
    }

    public void LoadLevel(string levelName)
    {
        StartCoroutine(LoadLevelCo(levelName));
    }

    IEnumerator LoadLevelCo(string levelName)
    {
        //play the animation
        transition.SetTrigger("ExitLevel");
        //wait for the animation to finish
        yield return new WaitForSeconds(animTime);

        //Load scene
        //op stores the progress of the current operation
        AsyncOperation op = SceneManager.LoadSceneAsync(levelName);
        loadScreen.SetActive(true);
        while (!op.isDone)
        {
            //wait until next frame
            yield return null;
        }
    }
}
