using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneTransition : MonoBehaviour
{
    private static GameObject _fader;
    private static LevelLoader _loader;

    public static void FadeToScene(string scene)
    {
        if (_fader == null) {
            GenerateFader();
        }

        _loader.LoadLevel(scene);
    }

    private static void GenerateFader()
    {
        GameObject canvas = new GameObject();
        canvas.transform.name = "[Runtime Generated] Scene Transitions";
        canvas.AddComponent<Canvas>();
        canvas.AddComponent<CanvasRenderer>();
        CanvasGroup g = canvas.AddComponent<CanvasGroup>();
        g.alpha = 0.0f;
        Animator animator = canvas.AddComponent<Animator>();
        animator.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("Anims/LevelLoading/FadeToBlack");
        Image i = canvas.AddComponent<Image>();
        i.color = Color.black;
        LevelLoader l = canvas.AddComponent<LevelLoader>();
        l.loadScreen = canvas;
        l.transition = animator;
        DontDestroyOnLoad(canvas);

        _fader = canvas;
        _loader = l;
    }
}
