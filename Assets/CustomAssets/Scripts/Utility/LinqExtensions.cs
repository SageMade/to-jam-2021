using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public static class LinqExtensions
{
    public static T TakeRandom<T>(this IEnumerable<T> source)
    {
        return source.Count() == 0 ?
            default(T) :
            source.ElementAt(Random.Range(0, source.Count()));
    }
}