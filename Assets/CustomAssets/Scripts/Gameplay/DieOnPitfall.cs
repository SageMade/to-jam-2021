using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterBase))]
public class DieOnPitfall : MonoBehaviour
{
    [Layer]
    public int DeathPitLayer;

    CharacterBase _character;

    private void Start() {
        _character = GetComponent<CharacterBase>();
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.layer == DeathPitLayer) {
            _character.health = 0.0f;
        }
    }
}
