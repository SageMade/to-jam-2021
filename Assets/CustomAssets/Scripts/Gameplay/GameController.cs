using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class GameController : MonoBehaviour
{
    public float RunTime = 20.0f;
    public LevelGenerator LevelGen;
    public RewindQueue RewindController;
    public TriggerDispatch ExitStartingRoomTrigger;
    public GameObject Player;
    public GameObject LightRig;
    [Level]
    public string ExitScene;

    [Header("SFX")]
    public StudioEventEmitter RewindVoiceLines;
    public StudioEventEmitter StartingVoiceLines;

    public string OutOfTimeParamName = "Running out of time";

    public bool HasTimerStarted = false;
    public int BaseStackCount = 10;

    public UnityEngine.UI.Image TimerImage;
    public UnityEngine.UI.Slider Health;

    public SO_CharacterState DisabledState;
    public SO_CharacterState DefaultState;

    private CharacterBase _character;
    private Vector3 _spawnLoc;

    public float TimeRemaining {
        get;
        protected set;
    } = 60.0f;

    private Light[] _playerLights;

    // Start is called before the first frame update
    void Start() {
        LevelGen.StackCount = (int)(BaseStackCount * 1.0f + (GameEngine.Difficulty / 3.0f));
        LevelGen.GenerateLevel();
        LevelGen.ExitDispatch.OnTriggerEnterEvent.AddListener((x) => {
            if (x.CompareTag("Player")) FinishGame();
        });
        RewindController.RewindCompletedEvent.AddListener(ResetGame);
        ExitStartingRoomTrigger.OnTriggerEnterEvent.AddListener(OnExitRoom);
        _playerLights = LightRig.GetComponentsInChildren<Light>();
        _character = Player.GetComponent<CharacterBase>();

        ResetGame();
        GameEngine.CurrentRun = new GameEngine.GameStats();

        _spawnLoc = Player.transform.position;
    }

    private void ResetGame()
    {
        TimeRemaining = RunTime;
        _character.health = 1.0f;
        _character.velocity = Vector3.zero;
        _character.StartState(DefaultState);
        _character.GetComponent<CharacterController>().enabled = true;
        Player.transform.position = _spawnLoc;

        StartingVoiceLines.Play();

        GameEngine.CurrentRun.Rewinds++;
        LevelGen.BGMEmitter.SetParameter(OutOfTimeParamName, 0);

        for (int ix = 0; ix < _playerLights.Length; ix++) {
            _playerLights[ix].enabled = true;
        }
    }

    private void FinishGame()
    {
        HasTimerStarted = false;
        LevelGen.BGMEmitter.Stop();
        GameEngine.CurrentRun.CompletionTime = RunTime - TimeRemaining;
        SceneTransition.FadeToScene(ExitScene);
    }

    void OnExitRoom(Collider collider) {
        HasTimerStarted = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (HasTimerStarted) {
            TimeRemaining -= Time.deltaTime;

            if (TimeRemaining <= 0.0f) {
                HandleDeath();
            }
        }
        if (_character.health <= 0.0f) {
            HandleDeath();
        }
        TimerImage.fillAmount = TimeRemaining / RunTime;
        for (int ix = 0; ix < _playerLights.Length; ix++) {
            _playerLights[ix].enabled = (ix / (float)_playerLights.Length) < TimerImage.fillAmount;
        }
        if (TimerImage.fillAmount < 0.2f) {
            LevelGen.BGMEmitter.SetParameter(OutOfTimeParamName, 1);
        }
        Health.value = _character.health;
    }

    void HandleDeath()
    {
        if (HasTimerStarted) {
            HasTimerStarted = false;
            RewindVoiceLines.Play();
            RewindController.Rewind();
            _character.GetComponent<CharacterController>().enabled = false;
            _character.StartState(DisabledState);
        }
    }
}
