using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LevelGenerator : MonoBehaviour
{
    public StageSet GenerationSet;
    public GameObject MusicTransitionPrefab;
    public string LevelSeed;

    public TriggerDispatch ExitDispatch;

    public int StackCount = 10;
    public bool GenerateOnStart = true;

    [Header("Music")]
    public FMODUnity.StudioEventEmitter BGMEmitter;
    [FMODUnity.ParamRef]
    public string AreaParamName;

    protected Random.State _generatorState;

    // Start is called before the first frame update
    void Start()
    {
        if (string.IsNullOrWhiteSpace(LevelSeed))
        {
            LevelSeed = System.Guid.NewGuid().ToString();
        }
        Random.InitState(LevelSeed.GetHashCode());
        _generatorState = Random.state;

        if (GenerateOnStart) {
            GenerateLevel();
        }
    }

    GameObject GenMusicTrigger(Transform parent, int area, Vector3 pos, Vector3 bounds) {
        GameObject result = Instantiate(MusicTransitionPrefab);
        result.transform.parent = parent;
        result.transform.localPosition = pos;
        result.transform.rotation = Quaternion.identity;
        result.GetComponent<BoxCollider>().size = bounds;
        MusicTriggerRegion region = result.GetComponent<MusicTriggerRegion>();
        region.BGMEmitter = BGMEmitter;
        region.ParamName = AreaParamName;
        region.ParamValue = area;
        return result;
    }

    /// <summary>
    /// Generates all stage stacks for the level
    /// </summary>
    public void GenerateLevel()
    {
        Random.state = _generatorState;

        int ix = 0;
        for (; ix < StackCount - 1; ix++) {
            GenerateStack(ix, GenerationSet.MainPrefabs.TakeRandom());
        }
        GameObject mainPrefab = GenerationSet.MainPrefabs.Where(HasBothConnections).TakeRandom();
        GenerateStack(ix, mainPrefab);

        GameObject final = GameObject.Instantiate(GenerationSet.EndPrefabs.TakeRandom());
        final.transform.parent = transform;
        final.transform.name = "EndGoal";
        final.transform.localPosition = new Vector3(0.0f, 0.0f, StackCount * StageController.HalfWidth * 2.0f - StageController.HalfWidth);

        ExitDispatch = final.GetComponent<EndRoomController>().EndTrigger.GetComponent<TriggerDispatch>();
    }

    void GenerateStack(int ix, GameObject mainPrefab)
    {
        // Create a root object for the stack
        GameObject stack = new GameObject("Stack_" + ix);
        stack.transform.parent = transform;
        stack.transform.localPosition = Vector3.zero;
        stack.transform.localRotation = Quaternion.identity;

        // Get the main section's stage controller
        StageController controller = mainPrefab.GetComponent<StageController>();
        Debug.Assert(controller, "");

        // Select a rafters prefab
        GameObject raftersPrefab = null;
        if (controller.HasTopConnection) {
            raftersPrefab = GenerationSet.RafterPrefabs.Where(HasBottomConnection).TakeRandom();
        } else {
            raftersPrefab = GenerationSet.RafterPrefabs.Where(DoesNotHaveBottomConnection).TakeRandom();
        }

        // Select a basement prefab
        GameObject basementPrefab = null;
        if (controller.HasBottomConnection) {
            basementPrefab = GenerationSet.BasementPrefabs.Where(HasTopConnection).TakeRandom();
        } else {
            basementPrefab = GenerationSet.BasementPrefabs.Where(DoesNotHaveTopConnection).TakeRandom();
        }

        StageController raftersCtrl  = raftersPrefab.GetComponent<StageController>();
        StageController basementCtrl = basementPrefab.GetComponent<StageController>();

        /////////////////////////////////////////////////////////////////////////
        ///////////////////////// Object Creation ///////////////////////////////
        /////////////////////////////////////////////////////////////////////////

        // Position the stack in world space
        Vector3 pos = stack.transform.localPosition;
        pos.z = ix * StageController.HalfWidth * 2.0f;
        stack.transform.localPosition = pos;

        // Create an instance of the main stage area prefab
        GameObject mainStage = GameObject.Instantiate(mainPrefab);
        mainStage.transform.parent = stack.transform;
        mainStage.transform.localPosition = Vector3.zero;
        mainStage.name = "main";

        // Main In Trigger
        {
            Vector3 inDoorPos = Vector3.zero;
            inDoorPos.z = -StageController.HalfWidth + 0.5f;
            inDoorPos.y = StageController.DoorHeight / 2.0f;
            Vector3 bounds = new Vector3(1.0f, StageController.DoorHeight, StageController.DoorWidth);
            GenMusicTrigger(stack.transform, controller.ThemeTune == MusicTheme.Platformer ? 1 : 2, inDoorPos, bounds);
        }
        if (controller.HasBottomConnection)
        {
            Vector3 inDoorPos = Vector3.zero;
            inDoorPos.z = 0;
            inDoorPos.y = -StageController.MainHalfHeight + 0.5f;
            Vector3 bounds = new Vector3(StageController.InterchangeLength, 1.0f, StageController.InterchangeWidth);
            GenMusicTrigger(stack.transform, controller.ThemeTune == MusicTheme.Platformer ? 1 : 2, inDoorPos, bounds);
        }
        if (controller.HasTopConnection)
        {
            Vector3 inDoorPos = Vector3.zero;
            inDoorPos.z = 0;
            inDoorPos.y = StageController.MainHalfHeight - 0.5f;
            Vector3 bounds = new Vector3(StageController.InterchangeLength, 1.0f, StageController.InterchangeWidth);
            GenMusicTrigger(stack.transform, controller.ThemeTune == MusicTheme.Platformer ? 1 : 2, inDoorPos, bounds);
        }

        // Create and position an instance of the basement stage
        GameObject basementStage = GameObject.Instantiate(basementPrefab);
        basementStage.transform.parent = stack.transform;
        basementStage.transform.localPosition = new Vector3(0, -(StageController.MainHalfHeight + StageController.BasementHalfHeight), 0.0f);
        basementStage.name = "basement";

        // Basement in Trigger
        {
            Vector3 inDoorPos = Vector3.zero;
            inDoorPos.z = -StageController.HalfWidth + 0.5f;
            inDoorPos.y = basementStage.transform.localPosition.y + StageController.DoorHeight / 2.0f;
            Vector3 bounds = new Vector3(1.0f, StageController.DoorHeight, StageController.DoorWidth);
            GenMusicTrigger(stack.transform, basementCtrl.ThemeTune == MusicTheme.Platformer ? 1 : 2, inDoorPos, bounds);
        }
        if (basementCtrl.HasTopConnection) 
        {
            Vector3 inDoorPos = basementStage.transform.localPosition;
            inDoorPos.y += StageController.BasementHalfHeight - 0.5f;
            Vector3 bounds = new Vector3(StageController.InterchangeLength, 1.0f, StageController.InterchangeWidth);
            GenMusicTrigger(stack.transform, basementCtrl.ThemeTune == MusicTheme.Platformer ? 1 : 2, inDoorPos, bounds);
        }

        // Crate and position an instance of the rafters stage
        GameObject raftersStage = GameObject.Instantiate(raftersPrefab);
        raftersStage.transform.parent = stack.transform;
        raftersStage.transform.localPosition = new Vector3(0, (StageController.MainHalfHeight + StageController.RafterHalfHeight), 0.0f);
        raftersStage.name = "rafters";


        // Rafters in Trigger
        {
            Vector3 inDoorPos = Vector3.zero;
            inDoorPos.z = -StageController.HalfWidth + 0.5f;
            inDoorPos.y = raftersStage.transform.localPosition.y + StageController.DoorHeight / 2.0f;
            Vector3 bounds = new Vector3(1.0f, StageController.DoorHeight, StageController.DoorWidth);
            GenMusicTrigger(stack.transform, raftersCtrl.ThemeTune == MusicTheme.Platformer ? 1 : 2, inDoorPos, bounds);
        }
        if (raftersCtrl.HasBottomConnection)
        {
            Vector3 inDoorPos = raftersStage.transform.localPosition;
            inDoorPos.y -= StageController.RafterHalfHeight - 0.5f;
            Vector3 bounds = new Vector3(StageController.InterchangeLength, 1.0f, StageController.InterchangeWidth);
            GenMusicTrigger(stack.transform, raftersCtrl.ThemeTune == MusicTheme.Platformer ? 1 : 2, inDoorPos, bounds);
        }
    }


    bool HasTopConnection(GameObject prefab) {
        StageController controller = prefab.GetComponent<StageController>();
        Debug.Assert(controller);
        return controller.HasTopConnection;
    }
    bool DoesNotHaveTopConnection(GameObject prefab) {
        StageController controller = prefab.GetComponent<StageController>();
        Debug.Assert(controller);
        return !controller.HasTopConnection;
    }
    bool HasBottomConnection(GameObject prefab)  {
        StageController controller = prefab.GetComponent<StageController>();
        Debug.Assert(controller);
        return controller.HasBottomConnection;
    }
    bool DoesNotHaveBottomConnection(GameObject prefab) {
        StageController controller = prefab.GetComponent<StageController>();
        Debug.Assert(controller);
        return !controller.HasBottomConnection;
    }
    bool HasBothConnections(GameObject prefab) {
        StageController controller = prefab.GetComponent<StageController>();
        Debug.Assert(controller);
        return controller.HasBottomConnection & controller.HasTopConnection;
    }
}