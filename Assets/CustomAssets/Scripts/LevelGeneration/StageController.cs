using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageController : MonoBehaviour
{

    public static float HalfWidth = 50.0f;
    public static float MainHalfHeight = 11.0f;
    public static float RafterHalfHeight = 8.0f;
    public static float BasementHalfHeight = 5.0f;
    public static float DoorHeight = 4.0f;
    public static float DoorWidth = 3.0f;

    public static float InterchangeLength = 10.0f;
    public static float InterchangeWidth = 4.0f;

    [Tooltip("Where in the stack this stage can generate")]
    public StageItemType Type;
    [Tooltip("Whether this stage has an exit along the top that can connect to the other levels (ignored for rafters)")]
    public bool HasTopConnection;
    [Tooltip("Whether this stage has an exit along the bottom that can connect to the other levels (ignored for basements)")]
    public bool HasBottomConnection;

    [Tooltip("The theme to play when the user is within this stage")]
    public MusicTheme ThemeTune = MusicTheme.Platformer;

    private void OnDrawGizmosSelected()
    {
        float HalfHeight = 0.0f;


        switch (Type)
        {
            case StageItemType.MainFloor: HalfHeight = MainHalfHeight; break;
            case StageItemType.Rafters:   HalfHeight = RafterHalfHeight; break;
            case StageItemType.Basement:  HalfHeight = BasementHalfHeight; break;
        }
        float doorCenter = (DoorHeight / 2.0f);
        Vector3 top = transform.localToWorldMatrix.MultiplyPoint(new Vector3(0.0f, HalfHeight, 0.0f));
        Vector3 bottom = transform.localToWorldMatrix.MultiplyPoint(new Vector3(0.0f, -HalfHeight, 0.0f));
        Vector3 left = transform.localToWorldMatrix.MultiplyPoint(new Vector3(-HalfWidth, doorCenter, 0.0f));
        Vector3 right = transform.localToWorldMatrix.MultiplyPoint(new Vector3(HalfWidth, doorCenter, 0.0f));
        Gizmos.color = Color.magenta;
        if (HasTopConnection && Type != StageItemType.Rafters)
            Gizmos.DrawWireCube(top, new Vector3(InterchangeLength, 1, InterchangeWidth));
        if (HasBottomConnection && Type != StageItemType.Basement)
            Gizmos.DrawWireCube(bottom, new Vector3(InterchangeLength, 1, InterchangeWidth));

        Gizmos.DrawWireCube(left, new Vector3(1, DoorHeight, DoorWidth));
        Gizmos.DrawWireCube(right, new Vector3(1, DoorHeight, DoorWidth));
    }
}
public enum MusicTheme {
    Platformer,
    Battle
}

public enum StageItemType
{
    MainFloor,
    Rafters,
    Basement
}
