using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Custom/Stage")]
public class StageSet : ScriptableObject {
    public List<GameObject> RafterPrefabs;
    public List<GameObject> MainPrefabs;
    public List<GameObject> BasementPrefabs;

    public List<GameObject> EndPrefabs;
}