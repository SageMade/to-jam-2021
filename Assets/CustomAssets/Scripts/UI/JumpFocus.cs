using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class JumpFocus : MonoBehaviour
{
    private void Start() {
        FindObjectOfType<EventSystem>().SetSelectedGameObject(gameObject);
    }

    private void OnEnable() {
        FindObjectOfType<EventSystem>().SetSelectedGameObject(gameObject);
    }
}
