using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndStatsController : MonoBehaviour
{
    public GameObject CollectablePrefab;
    [Level]
    public string MainMenu;

    [Header("UI Elements")]
    public GameObject CollectedContainer;
    public TMPro.TextMeshProUGUI Difficulty;
    public TMPro.TextMeshProUGUI CompletionTime;
    public TMPro.TextMeshProUGUI Rewinds;
    public UnityEngine.UI.Button QuitButton;


    private void Start()
    {
        QuitButton.onClick.AddListener(GoToMenu);

        Difficulty.text = GameEngine.Difficulty == 1 ? "Easy" : GameEngine.Difficulty == 2 ? "Medium" : "Hard";
        CompletionTime.text = GameEngine.CurrentRun.CompletionTime.ToString("0.00") + "s";
        Rewinds.text = GameEngine.CurrentRun.Rewinds.ToString();

        foreach (Collectable c in GameEngine.CurrentRun.CollectedItems) {
            GameObject card = Instantiate(CollectablePrefab, CollectedContainer.transform);
            card.GetComponent<CollectableCardController>().Item = c;
        }
    }

    private void GoToMenu()
    {
        SceneTransition.FadeToScene(MainMenu);
    }
}
