using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    public enum State {
        None,
        SelectDifficulty,
        ShowCollectables,
        ShowCredits,
        Quit
    }
    private State _state = State.None;

    public GameObject DifficultySelectPage;
    public GameObject CollectablesPage;
    public GameObject CreditsPage;

    public UnityEngine.UI.Button PlayButton;
    public UnityEngine.UI.Button CollectablesButton;
    public UnityEngine.UI.Button CreditsButton;
    public UnityEngine.UI.Button QuitButton;

    // Start is called before the first frame update
    void Start() {
        SetState(State.None);

        PlayButton.onClick.AddListener(() => { SetState(State.SelectDifficulty); });
        CollectablesButton.onClick.AddListener(() => { SetState(State.ShowCollectables); });
        CreditsButton.onClick.AddListener(() => { SetState(State.ShowCredits); });
        QuitButton.onClick.AddListener(() => { SetState(State.Quit); });
    }

    // Update is called once per frame
    public void SetState(State state) {
        _state = state;
        DifficultySelectPage.SetActive(false);
        CollectablesPage.SetActive(false);
        CreditsPage.SetActive(false);

        switch(_state) {
            case State.SelectDifficulty:
                DifficultySelectPage.SetActive(true);
                break;
            case State.ShowCollectables:
                CollectablesPage.SetActive(true);
                break;
            case State.ShowCredits:
                CreditsPage.SetActive(true);
                break;
            case State.Quit:
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#endif
                Application.Quit();
                break;

        }
    }
}
