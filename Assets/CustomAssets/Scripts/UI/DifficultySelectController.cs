using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultySelectController : MonoBehaviour
{
    [Level]
    public string SceneName;

    public UnityEngine.UI.Button EasyButton;
    public UnityEngine.UI.Button MediumButton;
    public UnityEngine.UI.Button HardButton;

    // Start is called before the first frame update
    void Start() {
        EasyButton.onClick.AddListener(() => { StartGame(1); });
        MediumButton.onClick.AddListener(() => { StartGame(2); });
        HardButton.onClick.AddListener(() => { StartGame(3); });
    }

    void StartGame(int difficulty) {
        GameEngine.Difficulty = difficulty;
        SceneTransition.FadeToScene(SceneName);
    }
}
