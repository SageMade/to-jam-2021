using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TEMP_Boolet : MonoBehaviour
{
    //
    public Vector3 direction;
    public bool enable = false;
    public float speed = 1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (enable)
        {
            transform.Translate(direction * speed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.name);
        if (other.name == "PlayerReal")
        {
            other.GetComponent<CharacterBase>().health -= 0.1f;
        }
    }
}
