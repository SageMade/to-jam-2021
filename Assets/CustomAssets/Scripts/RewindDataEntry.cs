using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct RewindDataEntry
{
    public float TimeStamp;
    public Vector3 Position;
    public Quaternion Rotation;
    public SO_CharacterState State;
    public float StateTimeCurrent;
    public Quaternion PlayerDir;
    // TODO:
    //    - Animation data
    //    - Velocity?
    //    - Scale?
}
