using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;

    // Update is called once per frame
    void LateUpdate() { 
        Quaternion rot = Quaternion.LookRotation(target.position - transform.position, Vector3.up);
        transform.rotation = rot;
    }
}
