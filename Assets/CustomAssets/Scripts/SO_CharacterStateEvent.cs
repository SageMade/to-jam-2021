using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "State Event", menuName = "Scriptable Objects/State Event", order = 1)]
[System.Serializable]
public class SO_CharacterStateEvent : ScriptableObject
{
    //
    public int eventID; // either store as an ID and cross ref it later to see which script to actually run
    // or store as a string to skip the cross ref, but will be a bit slower and less overhead
}
