# Welcome to the OTAF TOJam 2021 Repo

### Notes for Contributors:

In order to keep a clear seperation between our orignal assets and those from packages, all assets created for the project should be under the *Assets/CustomAssets* folder.

The FMOD Studio project will be located in the *FMODStudioProject* folder in the root of the repo.

Due to the large number of programmers, if will be important to minimize merge conflicts during our work flow. As such, the *master* branch has been made a protected branch. This means that all work should be done in a seperate branch and merged into the main branch either via a [Merge Request](https://gitlab.com/SageMade/to-jam-2021/-/merge_requests) or by asking the project maintainer (Sage) to merge the requested branch.

If you are unfamiliar with the process of merge requests, the flow should look like this:
1. Commit all local changes to your branch
2. Pull master and merge master into *your* branch
3. Resolve any merge conflicts
4. Push your branch to remote
5. Create a merge request in GitLab and ping the project maintainer to OK the merge

We may add extra people who can OK merge requests if required, but the goal is to minimize conflicts when merging between branches.

Also, due to the nature of Unity scene serialization, please coordinate changes to scenes with the rest of the team, as these are the most common areas for merge issues (and some of the hardest to resolve). For best resolving issues with scene merging, I highly reccomend configuring your git tool of choice to use the [Unity Smart Merge Tool](https://docs.unity3d.com/Manual/SmartMerge.html)

For instance, configuring with Git Fork can be seen below:
![Git Fork Unity SmartMerge Config](https://gitlab.com/SageMade/to-jam-2021/uploads/a7709803599b40fb083bf6393c6e16c7/gitfork.PNG)
